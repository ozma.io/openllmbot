{
  lib,
  stdenv,
  poetry2nix,
  pyright,
  python,
  pkgs,
  rustPackages,
}: let
  pkg = poetry2nix.mkPoetryApplication {
    projectDir = ./.;
    python = python;
    nativeBuildInputs = [pkgs.poetry pkgs.ruff pkgs.pyright];
    overrides = poetry2nix.overrides.withDefaults (self: super: {
      tiktoken = super.tiktoken.overridePythonAttrs (old: {
        nativeBuildInputs =
          old.nativeBuildInputs
          or []
          ++ (with rustPackages; [
            rustc
            cargo
            rustPlatform.cargoSetupHook
            self.setuptools-rust
          ]);
        cargoDeps = rustPackages.rustPlatform.importCargoLock {
          lockFile = ./nix/tiktoken/Cargo.lock;
        };
        postPatch = ''
          ln -sf ${./nix/tiktoken/Cargo.lock} Cargo.lock
        '';
        cargoRoot = ".";
      });

      aiogram = super.aiogram.overridePythonAttrs (old: {
        nativeBuildInputs =
          old.nativeBuildInputs
          or []
          ++ [self.hatchling];
      });
    });
  };
in
  pkg
