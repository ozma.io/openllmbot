{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    poetry2nix.url = "github:nix-community/poetry2nix";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    self,
    nixpkgs,
    poetry2nix,
    flake-utils,
  }:
    {
      overlays.default = final: prev: {
        ozma-bot = final.python3.pkgs.callPackage ./. {};
      };
    }
    // flake-utils.lib.eachDefaultSystem (system: let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [
          poetry2nix.overlays.default
          self.overlays.default
        ];
      };
    in {
      packages.default = pkgs.ozma-bot;
      formatter = pkgs.alejandra;
    });
}
