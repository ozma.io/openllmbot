from functools import cache
import math
import asyncio
import logging
from datetime import datetime, timezone

from redis.commands.core import AsyncScript
import redis.asyncio as redis

from .utils import strip_text
from .supervisor import SupervisorChild


logger = logging.getLogger(__name__)


_QUEUE_POP_SCRIPT = """
    local queue_key = KEYS[1]
    local now = tonumber(ARGV[1])
    local values = redis.call('ZRANGEBYSCORE', queue_key, '-inf', now)
    if next(values) ~= nil then
        redis.call('ZREM', queue_key, unpack(values))
    end
    return values
"""


@cache
def _queue_pop_script():
    return strip_text(_QUEUE_POP_SCRIPT)


class Scheduler(SupervisorChild):
    _redis_pool: redis.ConnectionPool
    _stop_event: asyncio.Event
    _pending_tasks: set[asyncio.Task]
    _queue_key = "scheduler:queue"
    _script: AsyncScript | None = None

    def __init__(self, *, redis_pool: redis.ConnectionPool, queue_key: str | None = None):
        self._redis_pool = redis_pool
        if queue_key is not None:
            self._queue_key = queue_key
        self._stop_event = asyncio.Event()
        self._pending_tasks = set()

    async def process_task(self, now: datetime, id: str):
        raise NotImplementedError()

    async def _run_process_task(self, now: datetime, id: str):
        try:
            logger.info(f"Processing scheduler task for id {id}")
            await self.process_task(now, id)
        except Exception:
            logger.exception(f"Error in scheduler callback for id {id}")

    async def start(self):
        self._task = asyncio.create_task(self._poll())

    async def stop(self, task: asyncio.Task):
        self._stop_event.set()

    async def _poll(self):
        try:
            while True:
                time = datetime.now(timezone.utc)
                await self._check(time)
                curr_seconds = time.timestamp()
                # Wait until the next minute.
                next_seconds = math.ceil((curr_seconds + 1) / 60) * 60
                try:
                    await asyncio.wait_for(self._stop_event.wait(), timeout=next_seconds - curr_seconds)
                    break
                except TimeoutError:
                    pass
            await asyncio.gather(*self._pending_tasks)
        finally:
            self._stop_event.clear()

    async def _check(self, time: datetime):
        connection = redis.Redis(connection_pool=self._redis_pool)
        try:
            if self._script is None:
                self._script = connection.register_script(_queue_pop_script())
            raw_ids = await self._script(keys=[self._queue_key], args=[time.timestamp()])
            for raw_id in raw_ids:
                id = raw_id.decode("utf-8")
                task = asyncio.create_task(self._run_process_task(time, id))
                self._pending_tasks.add(task)
                task.add_done_callback(self._pending_tasks.remove)
        finally:
            await connection.close()

    async def _schedule(self, id: str, when: datetime, lt: bool):
        connection = redis.Redis(connection_pool=self._redis_pool)
        try:
            await connection.zadd(self._queue_key, {id: when.timestamp()}, lt=lt)
        finally:
            await connection.close()

    def schedule(self, id: str, when: datetime):
        return self._schedule(id, when, lt=True)

    async def deschedule(self, id: str):
        connection = redis.Redis(connection_pool=self._redis_pool)
        try:
            await connection.zrem(self._queue_key, id)
        finally:
            await connection.close()

    def reschedule(self, id: str, when: datetime):
        return self._schedule(id, when, lt=False)
