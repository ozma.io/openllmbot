import hashlib
import json
import logging
import re
import asyncio
from functools import cache, cached_property
from typing import Any, Awaitable, Callable, Iterable, TypeVar
from openai import AsyncOpenAI
from openai.types.chat import (
    ChatCompletion,
    ChatCompletionAssistantMessageParam,
    ChatCompletionMessageParam,
    ChatCompletionMessageToolCall,
    ChatCompletionMessageToolCallParam,
    ChatCompletionSystemMessageParam,
    ChatCompletionToolParam,
    ChatCompletionUserMessageParam,
)
from openai.types.chat.chat_completion_message_tool_call_param import (
    Function as ToolCallParamFunction,
)
from openai.types.chat.chat_completion_message_tool_call import (
    Function as ToolCallFunction,
)
from openai.types.chat.completion_create_params import ResponseFormat
from openai.types.shared_params import FunctionDefinition
from pydantic import BaseModel, TypeAdapter

from .utils import strip_text


logger = logging.getLogger(__name__)


def filter_json_schema(schema: Any) -> Any:
    if isinstance(schema, dict):
        # Filter uninformative `title` keys.
        return {name: filter_json_schema(value) for name, value in schema.items() if name != "title"}
    else:
        return schema


class FunctionTool(BaseModel):
    __function_name__: str
    __function_description__: str

    @classmethod
    def to_definition(cls):
        return ChatCompletionToolParam(
            type="function",
            function=FunctionDefinition(
                name=cls.__function_name__,
                description=cls.__function_description__,
                parameters=filter_json_schema(cls.model_json_schema()),
            ),
        )

    def to_tool_call_function(self):
        return ToolCallFunction(
            name=self.__function_name__,
            arguments=self.model_dump_json(),
        )

    def to_tool_call_param_function(self):
        return ToolCallParamFunction(
            name=self.__function_name__,
            arguments=self.model_dump_json(),
        )


def seed_from_string(user: str) -> int:
    return int.from_bytes(hashlib.md5(user.encode("utf-8")).digest()[:4], "big")


class OpenAIChatCompletion:
    _openai: AsyncOpenAI
    _default_model: str
    _user: str | None
    _seed: int | None

    def __init__(
        self, openai: AsyncOpenAI, default_model: str, *, user: str | None = None, seed: int | None = None
    ):
        self._openai = openai
        self._user = user
        self._seed = seed
        if default_model is not None:
            self._default_model = default_model

    async def __call__(
        self,
        messages: list[ChatCompletionMessageParam],
        *,
        model: str | None = None,
        response_format: ResponseFormat | None = None,
        tools: list[ChatCompletionToolParam] | None = None,
        **kwargs,
    ) -> ChatCompletion:
        if self._user is not None:
            kwargs["user"] = self._user
        if self._seed is not None:
            kwargs["seed"] = self._seed
        if response_format is not None:
            kwargs["response_format"] = response_format
        if tools is not None:
            kwargs["tools"] = tools
        return await self._openai.chat.completions.create(
            model=model or self._default_model,
            messages=messages,
            **kwargs,
        )


_FIX_JSON_PROMPT_START = """
    Fix a JSON so that it complies with the following JSON Schema:

   ```json
   {schema}
   ```
"""


@cache
def _fix_json_prompt_start():
    return strip_text(_FIX_JSON_PROMPT_START)


_FIX_JSON_REQUEST = """
    ```json
    {request}
    ```

    ```error
    {error}
    ````
"""


@cache
def _fix_json_request():
    return strip_text(_FIX_JSON_REQUEST)


_AnyBaseModel = TypeVar("_AnyBaseModel")
_AnyResult = TypeVar("_AnyResult")


class CustomValidationError(Exception):
    pass


async def validate_and_fix_custom_model(
    chat: OpenAIChatCompletion,
    type: type[_AnyBaseModel] | TypeAdapter[_AnyBaseModel],
    raw: str | _AnyBaseModel,
    post_validation: Callable[[_AnyBaseModel], Awaitable[_AnyResult]],
    *,
    max_retries: int | None = None,
    model: str | None = None,
    schema_details: str | None = None,
) -> _AnyResult:
    retries = max_retries or 3
    system_message: ChatCompletionSystemMessageParam | None = None

    adapter = type if isinstance(type, TypeAdapter) else TypeAdapter(type)
    raw_schema: Any = None

    while True:
        logger.info(f"Trying to validate a raw JSON input as {adapter.validator.title}: {raw}")
        try:
            if isinstance(raw, str):
                val = adapter.validate_json(raw)
            else:
                val = adapter.validate_python(raw)
            return await post_validation(val)
        except Exception as e:
            if retries <= 0:
                raise e
            retries -= 1
            logger.warn("Error while validating a raw JSON input, trying to fix", exc_info=e)

            if not raw_schema:
                raw_schema = filter_json_schema(adapter.json_schema())
            if not system_message:
                system_prompts = [_fix_json_prompt_start().format(schema=json.dumps(raw_schema))]
                if schema_details is not None:
                    system_prompts.append(schema_details)
                system_prompts.append("Respond with a fixed JSON.")
                system_prompt = "\n\n".join(system_prompts)
                system_message = ChatCompletionSystemMessageParam(role="system", content=system_prompt)
            messages = [
                system_message,
                ChatCompletionUserMessageParam(
                    role="user",
                    content=_fix_json_request().format(request=raw, error=e),
                ),
            ]
            raw_response = await chat(
                messages,
                response_format=ResponseFormat(type="json_object"),
                model=model,
            )
            response = raw_response.choices[0]
            if response.finish_reason == "length":
                raise RuntimeError("Context overflow while fixing the JSON")
            elif response.finish_reason == "stop":
                assert response.message.content is not None
                raw = response.message.content
            else:
                raise ValueError(f"Unexpected finish reason {response.finish_reason}")


async def _id_post_validation(val: _AnyBaseModel) -> _AnyBaseModel:
    return val


async def validate_and_fix_model(
    chat: OpenAIChatCompletion,
    typ: type[_AnyBaseModel] | TypeAdapter[_AnyBaseModel],
    raw: str | _AnyBaseModel,
    **kwargs,
) -> _AnyBaseModel:
    return await validate_and_fix_custom_model(chat, typ, raw, _id_post_validation, **kwargs)


CallId = str


class FunctionToolSet:
    _tools: dict[str, type[FunctionTool]]

    def __init__(self, tools: Iterable[type[FunctionTool]]):
        self._tools = {tool.__function_name__: tool for tool in tools}

    @property
    def tools(self):
        return self._tools

    @cached_property
    def definitions(self):
        return [tool.to_definition() for tool in self._tools.values()]

    async def validate_calls(
        self, chat: OpenAIChatCompletion, calls: Iterable[ChatCompletionMessageToolCall]
    ) -> list[tuple[CallId, FunctionTool]]:
        async def get_one(
            call: ChatCompletionMessageToolCall,
        ) -> tuple[str, FunctionTool]:
            assert call.type == "function"
            tool = self._tools.get(call.function.name)
            if tool is None:
                raise ValueError(f"Unknown function {call.function.name}")
            args = await validate_and_fix_model(chat, tool, call.function.arguments)
            return (call.id, args)

        return await asyncio.gather(*[get_one(call) for call in calls])


# Convert the calls into a history message.
def tool_calls_to_message(calls: list[tuple[CallId, FunctionTool]]) -> ChatCompletionAssistantMessageParam:
    return ChatCompletionAssistantMessageParam(
        role="assistant",
        tool_calls=[
            ChatCompletionMessageToolCallParam(type="function", id=id, function=call.to_tool_call_param_function())
            for id, call in calls
        ],
    )


@cache
def _code_block_regex():
    return re.compile(r"^```(?:\w+)?\s*\n(.*?)(?=^```)```", re.DOTALL | re.MULTILINE)


def extract_markdown_code_blocks(markdown: str) -> list[str]:
    return [match.group(1) for match in _code_block_regex().finditer(markdown)]
