import asyncio
import random
from types import TracebackType
from typing import AsyncContextManager

from .chat import ChatHandle


class InactivityMonitor(AsyncContextManager):
    _chat_handle: ChatHandle
    _inactivity_messages: list[str]
    _inactivity_interval = 90
    _task: asyncio.Task | None = None
    _typing: AsyncContextManager | None = None

    def __init__(
        self,
        *,
        chat_handle: ChatHandle,
        inactivity_messages: list[str],
        inactivity_interval: int | None = None,
    ):
        self._chat_handle = chat_handle
        self._inactivity_messages = inactivity_messages
        if inactivity_interval:
            self._inactivity_interval = inactivity_interval

    async def __aenter__(self):
        if self._task is not None:
            raise RuntimeError("Context manager is already running")
        typing = self._chat_handle.typing_text()
        await typing.__aenter__()
        task = asyncio.create_task(self._inactivity_loop())
        self._typing = typing
        self._task = task

    async def __aexit__(self, exc_type: type | None, exc: Exception | None, tb: TracebackType | None):
        try:
            if self._task is not None:
                self._task.cancel()
            if self._typing is not None:
                await self._typing.__aexit__(exc_type, exc, tb)
        finally:
            self._task = None
            self._typing = None

    async def _inactivity_loop(self):
        try:
            await asyncio.sleep(self._inactivity_interval)
            message = random.choice(self._inactivity_messages)
            await self._chat_handle.send_text(message)
        except asyncio.CancelledError:
            pass
