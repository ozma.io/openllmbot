import asyncio
from typing import Any
from pydantic import TypeAdapter
from aiohttp import web
import aiohttp
import logging

from ..chat import ChatDriver, ChatHandle, SimpleTextMessage
from ..utils import split_messages


IncomingMessage = SimpleTextMessage  # | URLVoiceMessage
_IncomingMessage = TypeAdapter(IncomingMessage)

logger = logging.getLogger(__name__)


class ChatwootDriver(ChatDriver):
    PLATFORM = "chatwoot"
    _chatwoot_token: str
    _chatwoot_url: str
    _chatwoot_account_id: int
    _host = "localhost"
    _port = 10000
    _app: web.Application
    _runner: web.AppRunner

    def __init__(
        self,
        *args,
        chatwoot_url: str,
        chatwoot_token: str,
        chatwoot_account_id: int,
        host: str | None,
        port: int | None,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self._chatwoot_url = chatwoot_url
        self._chatwoot_account_id = chatwoot_account_id
        self._chatwoot_token = chatwoot_token
        if host is not None:
            self._host = host
        if port is not None:
            self._port = port
        self._app = web.Application()
        self._app.add_routes([web.get("/", self._on_health_check), web.post("/hook", self._on_raw_message)])
        self._runner = web.AppRunner(self._app)

    async def _on_health_check(self, request: web.Request) -> web.Response:
        return web.Response(text="OK")

    async def _on_raw_message(self, request: web.Request) -> web.Response:
        message = await request.json()
        await self.on_message(message)
        return web.Response()

    # Type of `message` is described on https://www.chatwoot.com/docs/product/others/webhook-events
    async def on_message(self, message: Any) -> None:
        raise NotImplementedError()

    def get_message(self, message: str) -> IncomingMessage:
        return SimpleTextMessage(text=message)

    def deserialize_handle(self, raw_key: str) -> "ChatwootConversationHandle":
        converted_key: int | str = raw_key
        try:
            converted_key = int(converted_key)
        except ValueError:
            raise ValueError(f"Expected int, got {type(raw_key)}")
        return ChatwootConversationHandle(self, converted_key)

    def deserialize_incoming(self, raw: Any) -> IncomingMessage:
        return _IncomingMessage.validate_python(raw)

    async def start(self):
        await self._runner.setup()
        site = web.TCPSite(self._runner, self._host, self._port)
        return await site.start()

    async def stop(self, started_task: asyncio.Task):
        return await self._runner.cleanup()


class ChatwootConversationHandle(ChatHandle[ChatwootDriver]):
    id: int

    def __init__(self, driver: ChatwootDriver, conversation_id: int):
        super().__init__(driver)
        self.id = conversation_id

    def to_key(self):
        return str(self.id)

    # No API support
    #  def typing_text(self):
    #  return self.thread.typing()

    async def send_text(self, message: str) -> None:
        for chunk in split_messages(message, 10_000):
            logger.info("chatwoot chunks prepared")
            async with aiohttp.ClientSession() as session:
                url = f"{self.driver._chatwoot_url}/api/v1/accounts/{self.driver._chatwoot_account_id}/conversations/{self.id}/messages"
                logger.info(f"chatwoot trying to send to url: {url}")
                json_data = {"content": chunk, "message_type": "outgoing"}
                headers = {
                    "api_access_token": self.driver._chatwoot_token,
                    "Content-Type": "application/json; charset=utf-8",
                }
                async with session.post(url, headers=headers, json=json_data) as resp:
                    logging.info(f"chatwoot url: {url}")
                    #  logging.info(f"chatwoot request: {json_data}")
                    logging.info(f"chatwoot send_text response: {await resp.text()}")
                    pass

    async def lock_conversation(self) -> None:
        async with aiohttp.ClientSession() as session:
            url = f"{self.driver._chatwoot_url}/api/v1/accounts/{self.driver._chatwoot_account_id}/conversations/{self.id}/toggle_status"
            json_data = {"status": "resolved"}
            headers = {
                "api_access_token": self.driver._chatwoot_token,
                "Content-Type": "application/json; charset=utf-8",
            }
            async with session.post(url, headers=headers, json=json_data) as resp:
                logging.info(f"chatwoot url: {url}")
                #  logging.info(f"chatwoot request: {json_data}")
                logging.info(f"chatwoot send_text response: {await resp.text()}")
                pass
