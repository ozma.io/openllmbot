import asyncio
from typing import Any
import aiogram
from aiogram.utils.chat_action import ChatActionSender
from pydantic import BaseModel, TypeAdapter

from ..chat import (
    ChatDriver,
    ChatHandle,
    IncomingVoiceMessage,
    SimpleTextMessage,
    VoiceFormat,
)
from ..utils import split_messages


class _TelegramRawVoiceMessage(BaseModel):
    type: str = "voice"
    file_id: str


class TelegramVoiceMessage(IncomingVoiceMessage):
    bot: aiogram.Bot
    file_id: str

    def __init__(self, bot: aiogram.Bot, file_id: str):
        self.bot = bot
        self.file_id = file_id

    @property
    def format(self) -> VoiceFormat:
        return VoiceFormat.OGG

    def serialize(self) -> Any:
        return _TelegramRawVoiceMessage(file_id=self.file_id).model_dump()

    async def get_voice_bytes(self) -> bytes:
        file = await self.bot.get_file(self.file_id)
        if file.file_path is None:
            raise ValueError(f"File {self.file_id} not found")
        result = await self.bot.download_file(file.file_path)
        if result is None:
            raise ValueError(f"File {self.file_id} not found")
        return result.read()


IncomingMessage = SimpleTextMessage | TelegramVoiceMessage
_IncomingMessage = TypeAdapter(SimpleTextMessage | _TelegramRawVoiceMessage)


class TelegramDriver(ChatDriver):
    PLATFORM = "telegram"

    dispatcher: aiogram.Dispatcher
    bot: aiogram.Bot

    def __init__(self, *args, **kwargs):
        self.dispatcher = aiogram.Dispatcher()
        self.bot = aiogram.Bot(*args, **kwargs)

    def deserialize_handle(self, raw_key: str) -> "TelegramChatHandle":
        converted_key: int | str = raw_key
        try:
            converted_key = int(converted_key)
        except ValueError:
            pass
        return TelegramChatHandle(self, converted_key)

    def deserialize_incoming(self, raw: Any) -> IncomingMessage:
        converted = _IncomingMessage.validate_python(raw)
        if isinstance(converted, SimpleTextMessage):
            return converted
        elif isinstance(converted, _TelegramRawVoiceMessage):
            return TelegramVoiceMessage(bot=self.bot, file_id=converted.file_id)
        else:
            raise RuntimeError(f"Unexpected message type {type(converted)}")

    def get_message(self, message: aiogram.types.Message) -> IncomingMessage | None:
        if message.voice is not None:
            return TelegramVoiceMessage(bot=self.bot, file_id=message.voice.file_id)
        elif message.text is not None:
            return SimpleTextMessage(text=message.text)
        else:
            return None

    def start(self):
        return self.dispatcher.start_polling(self.bot, handle_signals=False)

    def stop(self, started_task: asyncio.Task):
        return self.dispatcher.stop_polling()


class TelegramChatHandle(ChatHandle[TelegramDriver]):
    chat_id: int | str

    def __init__(self, driver: TelegramDriver, chat_id: int | str):
        super().__init__(driver)
        self.chat_id = chat_id

    def to_key(self):
        return str(self.chat_id)

    def typing_text(self):
        return ChatActionSender.typing(bot=self.driver.bot, chat_id=self.chat_id)

    async def send_text(self, message: str) -> None:
        for chunk in split_messages(message, 4096):
            await self.driver.bot.send_message(chat_id=self.chat_id, text=chunk)
