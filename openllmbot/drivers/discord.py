import asyncio
from typing import Any
import discord
from pydantic import TypeAdapter

from ..chat import ChatDriver, ChatHandle, SimpleTextMessage, URLVoiceMessage
from ..utils import split_messages


IncomingMessage = SimpleTextMessage | URLVoiceMessage
_IncomingMessage = TypeAdapter(IncomingMessage)


class DiscordDriver(discord.Client, ChatDriver):
    PLATFORM = "discord"
    _token: str

    def __init__(self, *args, token: str, **kwargs):
        super().__init__(*args, **kwargs)
        self._token = token

    def deserialize_handle(self, raw_key: str) -> "DiscordThreadHandle":
        channel_id = int(raw_key)
        channel = self.get_channel(channel_id)
        if isinstance(channel, discord.Thread):
            return DiscordThreadHandle(self, channel)
        else:
            raise ValueError(f"Expected discord.Thread, got {type(channel)}")

    def deserialize_incoming(self, raw: Any) -> IncomingMessage:
        return _IncomingMessage.validate_python(raw)

    @staticmethod
    def get_message(message: discord.Message) -> IncomingMessage | None:
        # Detect voice messages.
        if message.content == "":
            if len(message.attachments) == 1 and (attachment := message.attachments[0]).filename == "voice-message.ogg":
                return URLVoiceMessage(url=attachment.url)
            else:
                # Some other attachment, ignore.
                return None
        else:
            return SimpleTextMessage(text=message.content)

    def start(self):
        return super().start(token=self._token)

    def stop(self, started_task: asyncio.Task):
        return super().close()


class DiscordThreadHandle(ChatHandle[DiscordDriver]):
    thread: discord.Thread

    def __init__(self, driver: DiscordDriver, thread: discord.Thread):
        super().__init__(driver)
        self.thread = thread

    def to_key(self):
        return str(self.thread.id)

    def typing_text(self):
        return self.thread.typing()

    async def send_text(self, message: str) -> None:
        await split_and_send(self.thread, message)

    async def lock_conversation(self) -> None:
        await self.thread.edit(locked=True)


async def split_and_send(channel: discord.TextChannel | discord.Thread, message: str, thread_name: str | None = None):
    first_message: discord.Message | None = None
    thread: discord.Thread | None = channel if isinstance(channel, discord.Thread) else None
    for i, chunk in enumerate(split_messages(message, 2000)):
        if i == 0:
            first_message = await channel.send(chunk)
        else:
            if thread is None:
                assert isinstance(channel, discord.TextChannel)
                assert first_message is not None
                if thread_name is None:
                    thread_name = "Long message from the bot"
                thread = await first_message.create_thread(name=thread_name, auto_archive_duration=60)
            await thread.send(chunk)
