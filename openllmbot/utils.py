import asyncio
import logging
import textwrap
from typing import Awaitable, Callable, Generator, TypeVar


logger = logging.getLogger(__name__)


def split_messages(message: str, max_length: int) -> Generator[str, None, None]:
    start = 0
    end = 0
    while start + max_length < len(message) and end != -1:
        end = message.rfind(" ", start, start + max_length + 1)
        yield message[start:end]
        start = end + 1
    yield message[start:]


T = TypeVar("T")


async def retry_if_fails(task: Callable[[], Awaitable[T]], *, max_retries: int = 1) -> T:
    retry = 0
    while True:
        try:
            return await task()
        except Exception as e:
            if retry >= max_retries:
                raise e
            retry += 1
            sleep_time = retry**2
            logger.exception(f"Task failed with error, retrying in {sleep_time} seconds")
            await asyncio.sleep(sleep_time)


def strip_text(prompt: str) -> str:
    return textwrap.dedent(prompt).strip()
