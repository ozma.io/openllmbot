from abc import ABC, abstractmethod
from copy import copy
import asyncio
import signal


class SupervisorChild(ABC):
    @abstractmethod
    async def start(self) -> None:
        raise NotImplementedError()

    async def stop(self, started_task: asyncio.Task) -> None:
        started_task.cancel()


class Supervisor:
    _children: list[SupervisorChild]
    _handling_signals = False
    _pending_tasks: set[asyncio.Task]
    _running_children: dict[SupervisorChild, asyncio.Task] | None = None

    def __init__(self, children: list[SupervisorChild] = []):
        self._children = copy(children)
        self._pending_tasks = set()

    def add_child(self, child: SupervisorChild) -> None:
        if self._running_children is not None:
            raise RuntimeError("Cannot add children while running")
        self._children.append(child)

    async def start(self, *, handle_signals=True) -> None:
        if self._running_children is not None:
            raise RuntimeError("Already started")

        self._running_children = {}

        if handle_signals:
            loop = asyncio.get_event_loop()
            loop.add_signal_handler(signal.SIGINT, self._handle_signal)
            loop.add_signal_handler(signal.SIGTERM, self._handle_signal)
            self._handling_signals = True

        for child in self._children:
            self._running_children[child] = asyncio.create_task(child.start())
        await asyncio.gather(*self._running_children.values())
        await asyncio.gather(*self._pending_tasks)

    async def stop(self) -> None:
        if self._running_children is None:
            return
        running_children = self._running_children
        self._running_children = None

        if self._handling_signals:
            loop = asyncio.get_event_loop()
            loop.remove_signal_handler(signal.SIGINT)
            loop.remove_signal_handler(signal.SIGTERM)
            self._handling_signals = False

        await asyncio.gather(
            *(child.stop(task) for child, task in running_children.items()), *running_children.values()
        )

    def _handle_signal(self):
        task = asyncio.create_task(self.stop())
        self._pending_tasks.add(task)
        task.add_done_callback(lambda t: self._pending_tasks.remove(t))
