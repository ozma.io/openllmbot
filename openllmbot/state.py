from functools import cache
from typing import Generic, TypeVar
import asyncio
import uuid
import logging
import redis.asyncio as redis
from redis.commands.core import AsyncScript
from pydantic import TypeAdapter

from .utils import strip_text


logger = logging.getLogger(__name__)


_START_REQUEST_SCRIPT = """
    local state_key = KEYS[1]
    local lock_key = KEYS[2]
    local queue_key = KEYS[3]
    local lock_id = ARGV[1]
    local lock_timeout = ARGV[2]
    local new_message = ARGV[3]
    if new_message ~= '' then
        redis.call('RPUSH', queue_key, new_message)
    end
    local success = redis.call('SET', lock_key, lock_id, 'NX', 'EX', lock_timeout)
    if not success then
        return nil
    end
    local state = redis.call('GET', state_key)
    local next_message = redis.call('LPOP', queue_key)
    return {state, next_message}
"""


@cache
def _start_request_script():
    return strip_text(_START_REQUEST_SCRIPT)


_NEXT_REQUEST_SCRIPT = """
    local state_key = KEYS[1]
    local lock_key = KEYS[2]
    local queue_key = KEYS[3]
    local lock_id = ARGV[1]
    local lock_timeout = ARGV[2]
    local new_state = ARGV[3]
    local curr_lock_id = redis.call('GET', lock_key)
    if curr_lock_id ~= lock_id then
        return nil
    end
    if new_state == '' then
        redis.call('DEL', state_key)
    else
        redis.call('SET', state_key, new_state)
    end
    local next_message = redis.call('LPOP', queue_key)
    if not next_message then
        redis.call('DEL', lock_key)
        return true
    else
        redis.call('EXPIRE', lock_key, lock_timeout)
        return next_message
    end
"""


@cache
def _next_request_script():
    return strip_text(_NEXT_REQUEST_SCRIPT)


_KEEPALIVE_SCRIPT = """
    local lock_key = KEYS[1]
    local lock_id = ARGV[1]
    local lock_timeout = ARGV[2]
    local curr_lock_id = redis.call('GET', lock_key)
    if curr_lock_id ~= lock_id then
        return false
    else
        redis.call('EXPIRE', lock_key, lock_timeout)
        return true
    end
"""


@cache
def _keepalive_script():
    return strip_text(_KEEPALIVE_SCRIPT)


_AnyRequest = TypeVar("_AnyRequest")
_AnyState = TypeVar("_AnyState")


_KEEPALIVE_INTERVAL = 10
_KEEPALIVE_TIMEOUT = 30


class StateManager(Generic[_AnyState, _AnyRequest]):
    _state_adapter: TypeAdapter[_AnyState]
    _request_adapter: TypeAdapter[_AnyRequest]
    _redis_pool: redis.ConnectionPool
    _prefix = "state"
    _start_request_script: AsyncScript | None = None
    _next_request_script: AsyncScript | None = None
    _keepalive_script: AsyncScript | None = None

    def __init__(
        self,
        *,
        state_type: type[_AnyState] | TypeAdapter[_AnyState],
        request_type: type[_AnyRequest] | TypeAdapter[_AnyRequest],
        redis_pool: redis.ConnectionPool,
        prefix: str | None = None,
    ):
        self._state_adapter = state_type if isinstance(state_type, TypeAdapter) else TypeAdapter(state_type)
        self._request_adapter = request_type if isinstance(request_type, TypeAdapter) else TypeAdapter(request_type)
        self._redis_pool = redis_pool
        if prefix is not None:
            self._prefix = prefix

    async def enqueue_request(self, id: str, new_request: _AnyRequest) -> None:
        connection = redis.Redis(connection_pool=self._redis_pool)
        try:
            if self._start_request_script is None:
                self._start_request_script = connection.register_script(_start_request_script())
            if self._next_request_script is None:
                self._next_request_script = connection.register_script(_next_request_script())

            script_keys = [
                f"{self._prefix}:data:{id}",
                f"{self._prefix}:lock:{id}",
                f"{self._prefix}:queue:{id}",
            ]
            raw_new_request = self._request_adapter.dump_json(new_request)
            lock_id = str(uuid.uuid4())
            ret = await self._start_request_script(
                keys=script_keys,
                args=[lock_id, _KEEPALIVE_TIMEOUT, raw_new_request],
                client=connection,
            )

            if ret is None:
                logger.debug(f"Another request is already being processed for {id}, postponing: {raw_new_request}")
                return

            raw_state, raw_request = ret
            lock = asyncio.Lock()
            keepalive_task = asyncio.create_task(self._keepalive(connection, lock, id, lock_id))
            try:
                if raw_state is None:
                    state = None
                else:
                    try:
                        state = self._state_adapter.validate_json(raw_state)
                    except Exception:
                        logger.exception(f"Invalid state for id {id}: {raw_state}")
                        state = None

                while True:
                    try:
                        logger.info(f"Processing next request for {id}: {raw_request}")
                        request = self._request_adapter.validate_json(raw_request)
                        state = await self.process_request(id, state, request)
                    except Exception:
                        logger.exception(f"Error during processing request for id {id}: {raw_request}")
                    raw_new_state = self._state_adapter.dump_json(state) if state is not None else ""

                    await lock.acquire()
                    ret = await self._next_request_script(
                        keys=script_keys,
                        args=[
                            lock_id,
                            _KEEPALIVE_TIMEOUT,
                            raw_new_state,
                        ],
                        client=connection,
                    )
                    if ret is None:
                        raise RuntimeError(f"Lock has vanished for id {id} during request: {raw_request}")
                    elif ret == 1:
                        return
                    else:
                        lock.release()
                        raw_request = ret
            finally:
                keepalive_task.cancel()
                try:
                    await keepalive_task
                except asyncio.CancelledError:
                    pass
        finally:
            await connection.close()

    async def _keepalive(self, connection: redis.Redis, lock: asyncio.Lock, id: str, lock_id: str) -> None:
        if self._keepalive_script is None:
            self._keepalive_script = connection.register_script(_keepalive_script())

        while True:
            await asyncio.sleep(_KEEPALIVE_INTERVAL)
            async with lock:
                ret = await self._keepalive_script(
                    keys=[f"{self._prefix}:lock:{id}"],
                    args=[lock_id, _KEEPALIVE_TIMEOUT],
                    client=connection,
                )
            if not ret:
                break

    async def process_request(self, id: str, state: _AnyState | None, request: _AnyRequest) -> _AnyState | None:
        raise NotImplementedError()
