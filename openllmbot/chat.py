from abc import ABC, abstractmethod
from contextlib import asynccontextmanager
from enum import StrEnum
import urllib.parse
import os.path
from typing import Any, AsyncContextManager, Generic, TypeVar
import aiohttp
from functools import cached_property

from pydantic import BaseModel

from .supervisor import Supervisor, SupervisorChild


@asynccontextmanager
async def _dummy_context_manager():
    yield None


class IncomingMessage(ABC):
    @abstractmethod
    def serialize(self) -> Any:
        raise NotImplementedError()


class IncomingTextMessage(IncomingMessage):
    text: str


class IncomingMessageModel(IncomingMessage, BaseModel):
    def serialize(self) -> Any:
        return self.model_dump()


class SimpleTextMessage(IncomingTextMessage, IncomingMessageModel):
    type: str = "text"
    text: str


class VoiceFormat(StrEnum):
    OGG = "ogg"
    MP3 = "mp3"

    @property
    def file_name(self) -> str:
        return f"voice.{self.value}"


def file_voice_format(file: str) -> VoiceFormat:
    bare_name, ext = os.path.splitext(file)
    if ext == ".opus" or ext == ".ogg":
        return VoiceFormat.OGG
    elif ext == ".mp3":
        return VoiceFormat.MP3
    else:
        raise ValueError(f"Unrecognized voice format: {file}")


def url_voice_format(url: str) -> VoiceFormat:
    name = urllib.parse.urlparse(url).path.split("/")[-1]
    return file_voice_format(name)


class IncomingVoiceMessage(IncomingMessage):
    @property
    def format(self) -> VoiceFormat:
        raise NotImplementedError()

    async def get_voice_bytes(self) -> bytes:
        raise NotImplementedError()


class URLVoiceMessage(IncomingVoiceMessage, IncomingMessageModel):
    type: str = "voice"
    url: str

    @cached_property
    def format(self) -> VoiceFormat:
        return url_voice_format(self.url)

    async def get_voice_bytes(self) -> bytes:
        async with aiohttp.request("GET", self.url) as response:
            response.raise_for_status()
            return await response.read()


_AnyChatDriver = TypeVar("_AnyChatDriver", bound="ChatDriver")


class ChatHandle(Generic[_AnyChatDriver], ABC):
    driver: _AnyChatDriver

    def __init__(self, driver: _AnyChatDriver):
        self.driver = driver

    @abstractmethod
    def to_key(self) -> str:
        raise NotImplementedError()

    def to_key_with_platform(self) -> str:
        return f"{self.driver.PLATFORM}:{self.to_key()}"

    @abstractmethod
    async def send_text(self, message: str) -> None:
        raise NotImplementedError()

    def typing_text(self) -> AsyncContextManager[None]:
        return _dummy_context_manager()

    async def lock_conversation(self) -> None:
        pass


class ChatDriver(SupervisorChild):
    PLATFORM: str

    @abstractmethod
    def deserialize_incoming(self, raw: Any) -> IncomingMessage:
        raise NotImplementedError()

    @abstractmethod
    def deserialize_handle(self, raw_key: str) -> ChatHandle:
        raise NotImplementedError()


class ChatDrivers:
    _supervisor: Supervisor
    _drivers: dict[str, ChatDriver]

    def __init__(self, *, supervisor: Supervisor):
        self._supervisor = supervisor
        self._drivers = {}

    def get_driver(self, platform: str) -> ChatDriver:
        return self._drivers[platform]

    def register_driver(self, driver: ChatDriver) -> None:
        if driver.PLATFORM in self._drivers:
            raise ValueError(f"Duplicate driver platform: {driver.PLATFORM}")
        if ":" in driver.PLATFORM:
            raise ValueError(f"Invalid platform name: {driver.PLATFORM}")
        self._supervisor.add_child(driver)
        self._drivers[driver.PLATFORM] = driver

    def deserialize_handle_with_platform(self, raw_key: str) -> ChatHandle:
        platform, key = raw_key.split(":", 1)
        return self.get_driver(platform).deserialize_handle(key)
